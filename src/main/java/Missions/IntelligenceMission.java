package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Missions.MissionItems.IntelligenceItem;

public class IntelligenceMission extends Mission{
    private String region;
    private Iintelligence iIntelligence;

    public IntelligenceMission(Coordinates myCoordinates, String myPilotName, AerialVehicle myAerailVehicle,String myRegion, Iintelligence myIintel)
    {
        super( myCoordinates, myPilotName, myAerailVehicle);
        region=myRegion;
        iIntelligence= (Iintelligence) myIintel;
    }

    public String executeMission(){
        return pilotName+":"+aerialVehicle.getPlaneName()+"Collecting Data in"
                +region+ " with:" +iIntelligence.getIntelligenceItem().getSensorType();
    }
}
