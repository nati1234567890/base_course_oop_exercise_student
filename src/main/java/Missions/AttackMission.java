package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Missions.MissionItems.AttackItem;

public class AttackMission extends Mission{
    private String target;
    private Iattack iattack;


    public AttackMission(Coordinates myCoordinates, String myPilotName, AerialVehicle myAerailVehicle
    ,String myTarget,Iattack myIAttack)
    {
        super( myCoordinates, myPilotName, myAerailVehicle);
        target=myTarget;
        iattack=myIAttack;
    }

    @Override
    public String executeMission() {
        return pilotName+":"+aerialVehicle.getPlaneName()+
                " Attacking"+target+ " with:" +
                iattack.getAttackItem().getBombType().toString()+ "X"+
                iattack.getAttackItem().getNumOfBombs();
    }
}
