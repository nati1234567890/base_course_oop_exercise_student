package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public class BdaMission extends Mission{
    private String objective;
    private Ibda ibda;

    public BdaMission(Coordinates myCoordinates, String myPilotName, AerialVehicle myAerailVehicle,String myObj,
                      Ibda myIbda)
    {
        super( myCoordinates, myPilotName, myAerailVehicle);
        objective=myObj;
        ibda=myIbda;
    }

    public String executeMission(){
        return pilotName+":"+aerialVehicle.getPlaneName()+" taking pictures of"
                +objective+ " with:" +ibda.getBdaItem().getCameraType()+"camera";
    }
}
