package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission{
    protected Coordinates missionDestination;
    protected String pilotName;
    protected AerialVehicle aerialVehicle;

    public Mission(Coordinates myCoordinates,String myPilotName,AerialVehicle myAerailVehicle)
    {
        missionDestination=myCoordinates;
        pilotName=myPilotName;
        aerialVehicle=myAerailVehicle;
    }

    public void begin(){
        System.out.println("Beginning mission!");
        aerialVehicle.flyTo(missionDestination);
    }
    public void cancel(){
        System.out.println("Abort mission!");
        aerialVehicle.land(aerialVehicle.getMotherBaseLocation());
    }
    public void finish(){
        System.out.println(executeMission());
        aerialVehicle.land(aerialVehicle.getMotherBaseLocation());
        System.out.println("Finished Mission!");
    }
    public abstract String executeMission();
}
