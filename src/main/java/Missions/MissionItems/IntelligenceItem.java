package Missions.MissionItems;

import Missions.MissionItems.ItemTypes.SensorType;

public class IntelligenceItem {
    private SensorType sensorType;

    public IntelligenceItem(SensorType mySensorType)
    {
        sensorType=mySensorType;
    }

    public String getSensorType() {
        return sensorType.toString();
    }
}
