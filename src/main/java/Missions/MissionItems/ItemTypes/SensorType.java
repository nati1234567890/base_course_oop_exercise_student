package Missions.MissionItems.ItemTypes;

public enum SensorType {
    INFRA_RED("INFRA_RED"),ELINT("ELINT");

    private final  String sensorType;

    SensorType(final String _sensorType){
        sensorType=_sensorType;
    }
}
