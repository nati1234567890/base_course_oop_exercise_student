package Missions.MissionItems.ItemTypes;

public enum BombType {
    PYTHON("PYTHON"),AMRAM("AMRAM"),SPICE250("SPICE250");

    private final String _bombType;

    BombType(final String bombType){
        _bombType =bombType;
    }

    @Override
    public String toString() {
        return _bombType;
    }
}
