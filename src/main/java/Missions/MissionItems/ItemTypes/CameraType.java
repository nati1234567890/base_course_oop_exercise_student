package Missions.MissionItems.ItemTypes;

public enum CameraType {
    REGULAR("REGULAR"),THERMAL("THERMAL"),NIGHT_VISION("NIGHT_VISION");

    private final String cameraType;
     CameraType(final String myCameraType){
        cameraType=myCameraType;
    }

    @Override
    public String toString() {
        return cameraType;
    }
}
