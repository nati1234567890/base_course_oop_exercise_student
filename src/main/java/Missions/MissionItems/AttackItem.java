package Missions.MissionItems;

import Missions.Iattack;
import Missions.MissionItems.ItemTypes.BombType;

public class AttackItem  {
    private int numOfBombs;
    private BombType bombType;

    public AttackItem(int numOfBombs, BombType bombType) {
        this.numOfBombs = numOfBombs;
        this.bombType = bombType;
    }

    public int getNumOfBombs() {
        return numOfBombs;
    }
    public BombType getBombType() {
        return bombType;
    }


}
