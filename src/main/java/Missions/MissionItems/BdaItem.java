package Missions.MissionItems;

import Missions.Ibda;
import Missions.MissionItems.ItemTypes.CameraType;

public class BdaItem  {
    private CameraType cameraType;

    public BdaItem(CameraType cameraType) {
        this.cameraType = cameraType;
    }

    public String getCameraType() {
        return cameraType.toString();
    }
}
