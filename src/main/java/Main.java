import AerialVehicles.AerialVehicle;
import AerialVehicles.AttackPlanes.F15;
import AerialVehicles.Catmams.Harons.Eitan;
import AerialVehicles.Catmams.Hermeses.Zik;
import AerialVehicles.FlightStatus;
import Entities.Coordinates;
import Missions.*;
import Missions.MissionItems.AttackItem;
import Missions.MissionItems.BdaItem;
import Missions.MissionItems.IntelligenceItem;
import Missions.MissionItems.ItemTypes.BombType;
import Missions.MissionItems.ItemTypes.CameraType;
import Missions.MissionItems.ItemTypes.SensorType;

public class Main {
    public static void main(String[] args) {

        int FLIGHT_HOURS=5;
        FlightStatus FLIGHT_STATUS_EITAN=FlightStatus.READY_TO_FLY;
        Coordinates eitan_coordinates=new Coordinates(30d,40d);
        Coordinates suspect_coordinate=new Coordinates(60d,70d);
        int MAX_FLIGHT_HOUR=1000;
        String MY_PLANES_NAME="MY planes";

        AerialVehicle myEitan=new Eitan(FLIGHT_HOURS,FLIGHT_STATUS_EITAN,eitan_coordinates,
                MAX_FLIGHT_HOUR,MY_PLANES_NAME,new IntelligenceItem(SensorType.ELINT));

        Mission myIntelligenceMissoin=new IntelligenceMission(
                suspect_coordinate,"Yaniv",myEitan,
                "iran Nuclear Lab ", (Iintelligence) myEitan);
        System.out.println("FIRST MISSION:");
        myIntelligenceMissoin.begin();
        myIntelligenceMissoin.executeMission();
        myIntelligenceMissoin.finish();

        int ATTACK_BOMB_NUMBER=20;
        String AttackPilotName="Yosi";
        BombType ATTACK_BombType=BombType.PYTHON;
        AerialVehicle f15=new F15(FLIGHT_HOURS,FLIGHT_STATUS_EITAN,eitan_coordinates,
                MAX_FLIGHT_HOUR,MY_PLANES_NAME,new AttackItem(ATTACK_BOMB_NUMBER,ATTACK_BombType));

        Mission myAttackMission=new AttackMission(suspect_coordinate,AttackPilotName,f15,"iran nuck", (Iattack) f15);
        System.out.println("SECOND MISSION:");
        myAttackMission.begin();
        myAttackMission.executeMission();
        myAttackMission.finish();


        AerialVehicle zik=new Zik(FLIGHT_HOURS,FLIGHT_STATUS_EITAN,eitan_coordinates,
                MAX_FLIGHT_HOUR,MY_PLANES_NAME,new IntelligenceItem(SensorType.ELINT),new BdaItem(CameraType.REGULAR));

        Mission myBDAMission=new BdaMission(suspect_coordinate,AttackPilotName,zik,"check", (Ibda) zik);
        System.out.println("THIRD MISSION");
        myBDAMission.begin();
        myBDAMission.executeMission();
        myBDAMission.finish();

    }


}
