package AerialVehicles.AttackPlanes;


import AerialVehicles.AttackPlanes.AttackPlane;
import AerialVehicles.FlightStatus;
import Entities.Coordinates;
import Missions.Ibda;
import Missions.MissionItems.AttackItem;
import Missions.MissionItems.BdaItem;

public class F16 extends AttackPlane implements Ibda {
    private final static String F16Name="F16";
    private BdaItem bdaItem;

    public F16(int flyHours, FlightStatus flightStatus, Coordinates motherBaseLocation,
               int MAX_FLIGHT_HOURS, String planeName, AttackItem MyAttackItem)
    {
        super( flyHours,  flightStatus,  motherBaseLocation,  MAX_FLIGHT_HOURS,  planeName,MyAttackItem);
        this.planeName=F16Name;
    }

    @Override
    public BdaItem getBdaItem() {
        return bdaItem;
    }
}
