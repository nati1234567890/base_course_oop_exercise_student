package AerialVehicles.AttackPlanes;

import AerialVehicles.AerialVehicle;
import AerialVehicles.FlightStatus;
import Entities.Coordinates;
import Missions.Iattack;
import Missions.MissionItems.AttackItem;

public class AttackPlane extends AerialVehicle implements Iattack {
    private int ATTACK_MAX_FLIGHT_HOURS = 250;
    private AttackItem attackItem;

    public AttackPlane(int flyHours, FlightStatus flightStatus, Coordinates motherBaseLocation,
                       int MAX_FLIGHT_HOURS, String planeName,AttackItem myAttackItem){
        super( flyHours,  flightStatus,  motherBaseLocation,  MAX_FLIGHT_HOURS,  planeName);
        this.MAX_FLIGHT_HOURS=ATTACK_MAX_FLIGHT_HOURS;
        attackItem=myAttackItem;
    }

    @Override
    public AttackItem getAttackItem() {
        return attackItem;
    }
}
