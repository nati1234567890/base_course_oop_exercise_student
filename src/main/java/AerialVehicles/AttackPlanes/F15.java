package AerialVehicles.AttackPlanes;


import AerialVehicles.FlightStatus;
import Entities.Coordinates;
import Missions.Iattack;
import Missions.Iintelligence;
import Missions.MissionItems.AttackItem;
import Missions.MissionItems.IntelligenceItem;

public class F15 extends AttackPlane implements Iintelligence {
    private final static String F15Name="F15";
    private IntelligenceItem intelligenceItem;



    public F15(int flyHours, FlightStatus flightStatus, Coordinates motherBaseLocation,
               int MAX_FLIGHT_HOURS, String planeName,AttackItem myAttackItem)
    {
        super( flyHours,  flightStatus,  motherBaseLocation,  MAX_FLIGHT_HOURS,  planeName,myAttackItem);
        this.planeName=F15Name;
    }

    @Override
    public IntelligenceItem getIntelligenceItem() {
        return intelligenceItem;
    }
}
