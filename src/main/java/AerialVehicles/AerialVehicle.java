package AerialVehicles;


import Entities.Coordinates;

public abstract class AerialVehicle {

    //flyHours-since last repair
    protected int flyHours;
    protected FlightStatus flightStatus;
    protected Coordinates motherBaseLocation;
    protected int MAX_FLIGHT_HOURS;
    protected String planeName;

    public AerialVehicle(int flyHours, FlightStatus flightStatus, Coordinates motherBaseLocation, int MAX_FLIGHT_HOURS, String planeName) {
        this.flyHours = flyHours;
        this.flightStatus = flightStatus;
        this.motherBaseLocation = motherBaseLocation;
        this.MAX_FLIGHT_HOURS = MAX_FLIGHT_HOURS;
        this.planeName = planeName;
    }

    public int getFlyHours() {
        return flyHours;
    }
    public FlightStatus getFlightStatus() {
        return flightStatus;
    }
    public Coordinates getMotherBaseLocation() {
        return motherBaseLocation;
    }
    public int getMAX_FLIGHT_HOURS() {
        return MAX_FLIGHT_HOURS;
    }
    public String getPlaneName() {
        return planeName;
    }

    public void flyTo(Coordinates destination){
        if (this.flightStatus==FlightStatus.READY_TO_FLY)
        {
            this.flightStatus=FlightStatus.ON_THE_AIR;
            System.out.println("Flying to "+destination.toString());
        }
        else if (this.flightStatus==FlightStatus.NOT_READY_TO_FLY)
        {
            System.out.println("Aerial Vehicle isn't ready to fly");

        }

    }
    public void land(Coordinates destination){
        System.out.println("Landing on "+destination.toString());
        check();
    }
    public void check(){
        if (this.flyHours>=this.MAX_FLIGHT_HOURS)
        {
            this.flightStatus=FlightStatus.NOT_READY_TO_FLY;
            this.repair();
        }
        else{
            this.flightStatus=FlightStatus.READY_TO_FLY;
        }
    }
    public void repair(){
        this.flyHours=0;
        this.flightStatus=FlightStatus.READY_TO_FLY;
    }
}
