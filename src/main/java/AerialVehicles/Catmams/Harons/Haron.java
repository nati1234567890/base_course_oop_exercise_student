package AerialVehicles.Catmams.Harons;

import AerialVehicles.Catmams.Catmam;
import AerialVehicles.FlightStatus;
import Entities.Coordinates;
import Missions.Iattack;
import Missions.MissionItems.AttackItem;
import Missions.MissionItems.IntelligenceItem;

public class Haron extends Catmam implements Iattack {
    private int HARON_MAX_FLIGHT_HOURS=150;
    private AttackItem attackItem;
    public Haron(int flyHours, FlightStatus flightStatus, Coordinates motherBaseLocation, int MAX_FLIGHT_HOURS, String planeName,IntelligenceItem _intelligenceItem , AttackItem _attackItem){
        super( flyHours,  flightStatus,  motherBaseLocation,  MAX_FLIGHT_HOURS, planeName,_intelligenceItem);
        this.MAX_FLIGHT_HOURS=HARON_MAX_FLIGHT_HOURS;
        attackItem=_attackItem;
    }

    public Haron(int flyHours, FlightStatus flightStatus, Coordinates motherBaseLocation, int MAX_FLIGHT_HOURS, String planeName,IntelligenceItem myIntelItem) {
        super( flyHours,  flightStatus,  motherBaseLocation,  MAX_FLIGHT_HOURS,  planeName,myIntelItem);
    }


    @Override
    public AttackItem getAttackItem() {
        return attackItem;
    }
}
