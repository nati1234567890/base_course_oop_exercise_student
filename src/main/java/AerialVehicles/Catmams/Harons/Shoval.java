package AerialVehicles.Catmams.Harons;


import AerialVehicles.FlightStatus;
import Entities.Coordinates;
import Missions.Ibda;
import Missions.MissionItems.AttackItem;
import Missions.MissionItems.BdaItem;
import Missions.MissionItems.IntelligenceItem;

public class Shoval extends Haron implements Ibda {
    private final static String ShovalName="Shoval";
    private BdaItem bdaItem;


    public Shoval(int flyHours, FlightStatus flightStatus, Coordinates motherBaseLocation, int MAX_FLIGHT_HOURS, String planeName,IntelligenceItem _intellItem, AttackItem _attackItem, BdaItem _bdaItem)
    {
        super( flyHours,  flightStatus,  motherBaseLocation,  MAX_FLIGHT_HOURS,  planeName,_intellItem,_attackItem);
        this.planeName=ShovalName;
    }

    public Shoval(int flyHours, FlightStatus flightStatus, Coordinates motherBaseLocation, int MAX_FLIGHT_HOURS, String planeName,IntelligenceItem myIntelItem)
    {
        super( flyHours,  flightStatus,  motherBaseLocation,  MAX_FLIGHT_HOURS,  planeName,myIntelItem);
    }

    @Override
    public BdaItem getBdaItem() {
        return bdaItem;
    }
}

