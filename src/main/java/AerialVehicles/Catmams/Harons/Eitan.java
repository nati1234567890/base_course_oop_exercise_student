package AerialVehicles.Catmams.Harons;

import AerialVehicles.Catmams.Harons.Haron;
import AerialVehicles.FlightStatus;
import Entities.Coordinates;
import Missions.MissionItems.AttackItem;
import Missions.MissionItems.IntelligenceItem;

public class Eitan extends Haron {
    private final static String EitanName="Eitan";

    public Eitan(int flyHours, FlightStatus flightStatus, Coordinates motherBaseLocation, int MAX_FLIGHT_HOURS, String planeName,IntelligenceItem _intelItem)
    {
        super( flyHours,  flightStatus,  motherBaseLocation,  MAX_FLIGHT_HOURS,  planeName,_intelItem);
        this.planeName=EitanName;
    }
}
