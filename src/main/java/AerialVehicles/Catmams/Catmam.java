package AerialVehicles.Catmams;

import AerialVehicles.AerialVehicle;
import AerialVehicles.FlightStatus;
import Entities.Coordinates;
import Missions.Iintelligence;
import Missions.MissionItems.IntelligenceItem;

public class Catmam extends AerialVehicle implements Iintelligence {
    private IntelligenceItem intelligenceItem;


    public Catmam(int flyHours, FlightStatus flightStatus, Coordinates motherBaseLocation, int MAX_FLIGHT_HOURS, String planeName,IntelligenceItem _intelligenceItem){
        super( flyHours,  flightStatus,  motherBaseLocation,  MAX_FLIGHT_HOURS,  planeName);
        intelligenceItem=_intelligenceItem;
    }

    public String  hoverOverLocation(Coordinates destination){
        this.flightStatus= FlightStatus.ON_THE_AIR;
        System.out.println("Hovering over:"+destination.toString());
        return "Hovering over"+destination.toString();
    }

    @Override
    public IntelligenceItem getIntelligenceItem() {
            return intelligenceItem;
    }
}
