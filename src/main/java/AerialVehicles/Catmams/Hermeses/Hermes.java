package AerialVehicles.Catmams.Hermeses;

import AerialVehicles.Catmams.Catmam;
import AerialVehicles.FlightStatus;
import Entities.Coordinates;
import Missions.Ibda;
import Missions.Iintelligence;
import Missions.MissionItems.BdaItem;
import Missions.MissionItems.IntelligenceItem;

public class Hermes extends Catmam implements Ibda {
    private int HERMES_MAX_FLIGHT_HOURS=100;
    private BdaItem bdaItem;

    public Hermes(int flyHours, FlightStatus flightStatus, Coordinates motherBaseLocation,
                  int MAX_FLIGHT_HOURS, String planeName,IntelligenceItem myIntelItem,BdaItem myBdaItem)
    {
        super( flyHours,  flightStatus,  motherBaseLocation,  MAX_FLIGHT_HOURS,  planeName,myIntelItem);
        this.MAX_FLIGHT_HOURS=HERMES_MAX_FLIGHT_HOURS;
        bdaItem=myBdaItem;

    }

    @Override
    public BdaItem getBdaItem() {
        return bdaItem;
    }
}
