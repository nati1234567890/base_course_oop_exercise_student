package AerialVehicles.Catmams.Hermeses;


import AerialVehicles.FlightStatus;
import Entities.Coordinates;
import Missions.Ibda;
import Missions.MissionItems.BdaItem;
import Missions.MissionItems.IntelligenceItem;

public class Zik extends Hermes  {
    private final static String ZikName="Zik";


    public Zik(int flyHours, FlightStatus flightStatus, Coordinates motherBaseLocation,
               int MAX_FLIGHT_HOURS, String planeName,IntelligenceItem myIntelItem,BdaItem myBdaItem)
    {
        super( flyHours,  flightStatus,  motherBaseLocation,  MAX_FLIGHT_HOURS,  planeName,myIntelItem,myBdaItem);
        this.planeName=ZikName;
    }


}