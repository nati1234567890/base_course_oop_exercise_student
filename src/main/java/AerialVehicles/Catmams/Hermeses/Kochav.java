package AerialVehicles.Catmams.Hermeses;


import AerialVehicles.FlightStatus;
import Entities.Coordinates;
import Missions.Iattack;
import Missions.MissionItems.AttackItem;
import Missions.MissionItems.BdaItem;
import Missions.MissionItems.IntelligenceItem;

public class Kochav extends Hermes implements Iattack {
    private final static String KochavName="Kochav";
    private AttackItem attackItem;


    public Kochav(int flyHours, FlightStatus flightStatus, Coordinates motherBaseLocation,
                  int MAX_FLIGHT_HOURS, String planeName, IntelligenceItem myIntelItem, BdaItem myBdaItem)
    {
        super( flyHours,  flightStatus,  motherBaseLocation,  MAX_FLIGHT_HOURS,  planeName,myIntelItem,myBdaItem);
        this.planeName=KochavName;
    }

    @Override
    public AttackItem getAttackItem() {
        return attackItem;
    }
}
